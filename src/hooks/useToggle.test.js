import { renderHook, act } from "@testing-library/react";
import { useToggle } from "./useToggle";

describe("useToggle", () => {
	it("should toggle the value correctly", () => {
		const { result } = renderHook(() => useToggle());

		expect(result.current[0]).toBe(false);

		act(() => {
			result.current[1]();
		});

		expect(result.current[0]).toBe(true);

		act(() => {
			result.current[1]();
		});

		expect(result.current[0]).toBe(false);
	});

	it("should initialize with the provided initial value", () => {
		const { result } = renderHook(() => useToggle(true));

		expect(result.current[0]).toBe(true);
	});
});
