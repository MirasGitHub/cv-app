export const proficiencyScaleElements = [
	{
		id: 1,
		level: "Beginner",
		className: "beginner",
	},
	{
		id: 2,
		level: "Proficient",
		className: "proficient",
	},
	{
		id: 3,
		level: "Expert",
		className: "expert",
	},
	{
		id: 4,
		level: "Master",
		className: "master",
	},
];
