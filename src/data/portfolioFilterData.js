export const portfolioFilterData = [
	{
		id: 1,
		type: "all",
		activeName: "All /",
	},
	{
		id: 2,
		type: "ui",
		activeName: "Ui /",
	},
	{
		id: 3,
		type: "code",
		activeName: "Code",
	},
];
