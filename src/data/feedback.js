export const feedback = [
	{
		feedback:
			"Outstanding work! Your attention to detail and commitment to delivering high-quality code have significantly improved our website. Your problem-solving skills and proactive approach make you an invaluable asset to the team. Your dedication and consistent ability to meet deadlines demonstrate a strong work ethic. Your dedication to continuous improvement and openness to feedback make you an invaluable asset to the team.",
		reporter: {
			photoUrl: "./assets/images/Ellipse 1.png",
			name: "John Doe",
			citeUrl: "https://www.citeexample.com",
		},
	},
	{
		feedback:
			"Exceptional developer! Your ability to translate complex requirements into elegant and efficient code is commendable. Your responsiveness and collaborative spirit make project collaboration seamless. Keep up the great work! Your positive attitude and willingness to take on challenges contribute to a positive team dynamic. Your problem-solving skills, proactive approach, and consistent ability to meet deadlines demonstrate a strong work ethic. ",
		reporter: {
			photoUrl: "./assets/images/Ellipse 1.png",
			name: "Andrew Drake",
			citeUrl: "https://www.citeexample.com",
		},
	},
];
