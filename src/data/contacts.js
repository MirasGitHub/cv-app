import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faSkype,
	faFacebookF,
	faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faPhone } from "@fortawesome/free-solid-svg-icons";

library.add(faFacebookF);

const mobileNum = "+995 557 420 520";
const email = "miranda.kobalia@yahoo.com";
const twitter = "https://twitter.com/wordpress";
const facebook = "https://www.facebook.com";
const skype = "miranda.kobalia";

export const contactInfo = [
	{
		id: 1,
		label: "",
		value: mobileNum,
		icon: faPhone,
		url: `tel:${mobileNum}`,
	},
	{
		id: 2,
		label: "",
		value: email,
		icon: faEnvelope,
		url: `mailto:${email}`,
	},
	{
		id: 3,
		label: "Twitter",
		value: twitter,
		icon: faTwitter,
		url: twitter,
	},
	{
		id: 4,
		label: "Facebook",
		value: facebook,
		icon: faFacebookF,
		url: facebook,
	},
	{
		id: 5,
		label: "Skype",
		value: skype,
		icon: faSkype,
		url: `skype:${skype}?chat`,
	},
];
