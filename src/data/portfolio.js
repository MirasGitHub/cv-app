export const portfolioData = [
	{
		id: 1,
		title: "Miranda's Portfolio project.",
		description:
			"This is a Miranda Kobalia's Portfolio Project developed in React js. Here the React-router-dom is used for routing through the pages. The visitor has the possibility to download Miranda's CV by clicking the download CV button.",
		link: "https://mirandakobalia-portfolio.netlify.app/",
		image: "/static/media/Portfolio%20cards.png",
		type: "code",
	},
	{
		id: 2,
		title: "Tip calculator",
		description:
			"The app called Tip Calculator that is responsible for calculating the personal as well as total tip by typing the bill amount and number of people. The design that has been implemented is from FIGMA design guidelines. The app is hosted on my Github profile and it is deployed on netlify.com.",
		link: "https://tip-calculator-mira.netlify.app",
		image: "/static/media/Portfolio%20cards.png",
		type: "code",
	},
	{
		id: 3,
		title: "Quiz App",
		description:
			"Quiz App is responsible for fetching the quiz questions asynchronously and each by one display on the screen. The user has the possibility to choose the correct answer where the latter is indicated as green and the rest in red colors. While fetching the questions, we can see the load spinner for more user friendly experience.",
		link: "https://quiz-ol-academy.netlify.app",
		image: "/static/media/Portfolio%20cardstwo.png",
		type: "code",
	},
	{
		id: 4,
		title: "Accordion Card",
		description:
			"The project is called Frequently Asked Questions' Accordion Card, which is the demonstration of React state management using useState React Hook. Whenever the user clicks on question container its answer is being displayed and another card that has been opened before is closed accordingly and vice versa.",
		link: "https://faq-accordion-card-with-react.netlify.app",
		image: "/static/media/Portfolio%20cards.png",
		type: "ui",
	},
	{
		id: 5,
		title: "React Form using React hook form library.",
		description:
			"The project is related to filling the form and submitting it. The Result is being demonstrated below the form in JSON format. The main tool that is used for managing the form state is called React Hook Form Library.",
		link: "https://gitlab.com/MirasGitHub/react-forms",
		image: "/static/media/Portfolio%20cardstwo.png",
		type: "ui",
	},
];
