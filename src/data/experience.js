export const experience = [
	{
		date: "2023 may-november",
		info: {
			company: "EPAM",
			job: "Front-end development, React.",
			description:
				"Studying in EPAM Upskill project we have touched upon different topics such as IT fundamentals, introduction to Web Development, markup languages such as HTML and CSS, Javascript basics and advanced topics. After learning some js advanced features, we have continued studying Typescript language and finally we moved on React where we learned all the basic as well as advanced features os the library.",
		},
	},
	{
		date: "2023 april-august",
		info: {
			company: "SkillWill",
			job: "Python development",
			description:
				"In SkillWill Academy I studied python language and all the basic fundamentals of it. Then we continued learning its framework Django where we had the possibility to build simple Rest APIs.",
		},
	},
	{
		date: "2023 may-july",
		info: {
			company: "Orient Logic",
			job: "React development",
			description:
				"In 4 month React development course we studied all the basics of the library. I had the opportunity to build some very interesting projects and use react tools.",
		},
	},
];
