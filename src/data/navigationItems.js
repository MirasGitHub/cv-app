import {
	faComment,
	faGem,
	faGraduationCap,
	faLocationArrow,
	faPen,
	faSuitcase,
	faUser,
} from "@fortawesome/free-solid-svg-icons";

export const navigationItems = [
	{ target: "about", icon: faUser, text: "About me" },
	{ target: "education", icon: faGraduationCap, text: "Education" },
	{ target: "experience", icon: faPen, text: "Experience" },
	{ target: "skills", icon: faGem, text: "Skills" },
	{ target: "portfolio", icon: faSuitcase, text: "Portfolio" },
	{ target: "contacts", icon: faLocationArrow, text: "Contacts" },
	{ target: "feedback", icon: faComment, text: "Feedback" },
];
