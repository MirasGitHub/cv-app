export const timeline = [
	{
		id: 1,
		date: 2023,
		title: "Front End, React Development",
		text: "Studying in EPAM Upskill project we have touched upon different topics such as IT fundamentals, introduction to Web Development, markup languages such as HTML and CSS, Javascript basics and advanced topics. After learning some js advanced features, we have continued studying Typescript language and finally we moved on React where we learned all the basic as well as advanced features os the library.",
	},
	{
		id: 2,
		date: 2023,
		title: "Python Development",
		text: "In SkillWill Academy I studied python language and all the basic fundamentals of it. Then we continued learning its framework Django where we had the possibility to build simple Rest APIs. Studying in SkillWill was a successful journey as I was ranked first place in the group. ",
	},
	{
		id: 3,
		date: 2023,
		title: "React Development",
		text: "In 4 month React development course we studied all the basics of the library. I had the opportunity to build some very interesting projects and use react tools. After completing our final project we had to present them in OL Academy office in front of other developers and the representatives of the Academy itself.",
	},
	{
		id: 4,
		date: 2022,
		title: "Javascript and Front-End Frameworks.",
		text: "GITA - ICT Trainings was one of the interesting parts of my learning adventure in IT field because I had the opportunity to study by the Bulgarian Lecturer and developer with a bunch of years of experience. At the end of the program we worked on final project that was related to hotels.",
	},
	{
		id: 5,
		date: 2020,
		title: "Front-End Angular Development",
		text: "In IT Step Academy Georgia, it was my first steps into IT field. At the beginning of the program we learned more about web development fundamentals, then we touched upon the front-end languages, the markup languages - html and css. After basics of html/css languages we started learning Javascript basics and finally moved on studying Angular Development and its fundamental features.",
	},
	{
		id: 6,
		date: 2020,
		title: "International Relations - Bachelor's degree.",
		text: "In Caucasus International University my major studying field was International Relations, which consisted of 4 academic year. During my bachelor studies I had the opportunity to work on some interesting topics and participate in different academic conferences. I also had the opportunity to study in other countries and participated in different exchange programs.",
	},
	{
		id: 7,
		date: 2015,
		title: "High school graduate",
		text: "Graduated Tbilisi Classic Gymnasium was one of the important and adventurous experiences in my life. I spent couple of years studying there which gave me the opportunity to meet up with my future friends and great people who taught us based on their knowledge and experiences.",
	},
];
