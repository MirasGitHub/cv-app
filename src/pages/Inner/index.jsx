import {
	Panel,
	Box,
	Expertise,
	Address,
	Feedback,
	TimeLine,
	Portfolio,
} from "../../components/index";
import "./Inner.scss";
import { Link } from "react-scroll";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";

import { timeline, content, experience, feedback } from "../../data/index";
import Skills from "../../components/Skills";
import { contactInfo } from "../../data/contacts";
import { useToggle } from "../../hooks/useToggle";

const Inner = () => {
	const [collapsed, setCollapsed] = useToggle(false);
	const [activeTab, setActiveTab] = useState("all");

	const handleCollapse = () => {
		setCollapsed((prevState) => !prevState);
	};

	return (
		<div
			className={`inner-page ${collapsed ? "collapsed" : null}`}
			data-testid="inner-component"
		>
			<aside>
				<section className="sidebar" data-testid="panel">
					<Panel collapsed={collapsed} onCollapse={handleCollapse} />
				</section>
			</aside>

			<section
				className="cv-details"
				id="cv-details-container"
				data-testid="about-me"
			>
				<section id="about">
					<Box title="About me" content={content} />
				</section>

				<section id="education" data-testid="education">
					<Box title="Education" name="education">
						<TimeLine data={timeline} />
					</Box>
				</section>

				<section id="experience" data-testid="experience">
					<Box title="Experience" name="experience">
						<Expertise data={experience} />
					</Box>
				</section>

				<section id="skills" data-testid="skills">
					<Box title="skills" name="skills">
						<Skills />
					</Box>
				</section>

				<section id="portfolio" data-testid="portfolio">
					<Box title="Portfolio" name="portfolio">
						<Portfolio activeTab={activeTab} setActiveTab={setActiveTab} />
					</Box>
				</section>

				<section id="contacts" data-testid="contacts">
					<Box title="Contacts" name="contacts">
						<Address contactInfo={contactInfo} />
					</Box>
				</section>

				<section id="feedback" data-testid="feedback">
					<Box title="Feedback">
						<Feedback data={feedback} />
					</Box>
				</section>

				<section className="scroll-up-button">
					<Link to="about" spy={true} smooth={true} offset={-50} duration={800}>
						<button>
							<FontAwesomeIcon icon={faChevronUp} />
						</button>
					</Link>
				</section>
			</section>
		</div>
	);
};

export default Inner;
