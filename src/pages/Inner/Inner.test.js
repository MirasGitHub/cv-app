import { render, screen } from "@testing-library/react";
import Inner from "./index";
import { MemoryRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../../app/store";
import axios from "axios";

jest.mock("axios");

describe("<Inner/>", () => {
	test("renders Panel component correctly", () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<Inner />
				</MemoryRouter>
			</Provider>
		);
		const panelComponent = screen.getByTestId("panel");
		expect(panelComponent).toBeInTheDocument();
	});

	test("renders About section correctly", () => {
		axios.get.mockRejectedValue(
			new Error("Request failed with status code 404")
		);

		render(
			<Provider store={store}>
				<MemoryRouter>
					<Inner />
				</MemoryRouter>
			</Provider>
		);
		const aboutMeSection = screen.getByTestId("about-me");

		expect(aboutMeSection).toBeInTheDocument();
	});

	test("renders Education section correctly", () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<Inner />
				</MemoryRouter>
			</Provider>
		);
		const educationSection = screen.getByTestId("education");

		expect(educationSection).toBeInTheDocument();
	});

	test("renders Experience section correctly", () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<Inner />
				</MemoryRouter>
			</Provider>
		);
		const experienceSection = screen.getByTestId("experience");

		expect(experienceSection).toBeInTheDocument();
	});

	test("renders Skills section correctly", () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<Inner />
				</MemoryRouter>
			</Provider>
		);
		const skillsSection = screen.getByTestId("skills");

		expect(skillsSection).toBeInTheDocument();
	});

	test("renders Portfolio section correctly", () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<Inner />
				</MemoryRouter>
			</Provider>
		);
		const portfolioSection = screen.getByTestId("portfolio");

		expect(portfolioSection).toBeInTheDocument();
	});

	test("renders Contacts section correctly", () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<Inner />
				</MemoryRouter>
			</Provider>
		);
		const contactsSection = screen.getByTestId("contacts");

		expect(contactsSection).toBeInTheDocument();
	});

	test("renders Feedback section correctly", () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<Inner />
				</MemoryRouter>
			</Provider>
		);
		const feedbackSection = screen.getByTestId("feedback");

		expect(feedbackSection).toBeInTheDocument();
	});
});
