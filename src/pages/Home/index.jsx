import { PhotoBox } from "../../components/PhotoBox";
import "./Home.scss";
import { Button } from "../../components/Button";
import profilePicture from "../../assets/images/miranda.jpeg";

const Home = () => {
	return (
		<div className="home-page" data-testid="home-component">
			<div className="photo-box" data-testid="photo-box">
				<PhotoBox
					name="Miranda Kobalia"
					title="Web Developer. React developer."
					description="Currently I am studying React js. as well as Back-end development tools such as Python and its framework django."
					avatar={profilePicture}
				/>
			</div>
			<div data-testid="knowMore">
				<Button text="Know more" path="/inner" />
			</div>
		</div>
	);
};

export default Home;
