import { fireEvent, render, screen } from "@testing-library/react";
import Home from "./index";
import { MemoryRouter } from "react-router-dom";

describe("<Home/>", () => {
	test("renders the Home component correctly", () => {
		render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);

		const homeComponent = screen.getByTestId("home-component");

		expect(homeComponent).toBeInTheDocument();
	});

	test("renders the PhotoBox component correctly", () => {
		render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);

		const photoBoxComponent = screen.getByTestId("photo-box");

		expect(photoBoxComponent).toBeInTheDocument();
	});

	test('navigates to the Inner page when the "Know more" button is clicked', () => {
		render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);

		const knowMoreButton = screen.getByTestId("knowMore");

		expect(knowMoreButton).toBeInTheDocument();

		fireEvent.click(knowMoreButton);
	});
});
