import React from "react";
import ReactDOM from "react-dom/client";
import "normalize.css";
import "./index.scss";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { makeServer } from "../src/services/server";
import { Provider } from "react-redux";
import store from "./app/store";

if (process.env.NODE_ENV === "development") {
	makeServer();
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
	<BrowserRouter>
		<Provider store={store}>
			<App />
		</Provider>
	</BrowserRouter>
);
