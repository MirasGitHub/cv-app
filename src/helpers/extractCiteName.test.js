import { extractCiteName } from "./extractCiteName";

describe("extractCiteName", () => {
	it("should correctly extract the domain name from a URL", () => {
		const testCases = [
			{
				url: "https://www.example.com/some-page",
				expected: "example.com",
			},
			{
				url: "http://subdomain.example.co.uk/path/to/resource",
				expected: "subdomain.example.co.uk",
			},
			{
				url: "https://www.another-example.org",
				expected: "another-example.org",
			},
			{
				url: "https://localhost:3000/page",
				expected: "localhost:3000",
			},
			{
				url: "https://127.0.0.1:8080/index.html",
				expected: "127.0.0.1:8080",
			},
		];

		testCases.forEach((testCase) => {
			const { url, expected } = testCase;
			const result = extractCiteName(url);
			expect(result).toBe(expected);
		});
	});
});
