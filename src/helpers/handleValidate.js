export const handleValidate = (values) => {
	const errors = {};
	if (!values.name) {
		errors.name = "Skill name is a required field";
	} else if (!values.range) {
		errors.range = "Skill range is a required field";
	}

	if (!/^\d+$/.test(values.range)) {
		errors.range = "Skill range must be a 'number' type";
	}

	if (/^\d+[a-zA-Z]/.test(values.range)) {
		errors.range = `Skill range must be a 'number' type but final value was: 'NaN' (cast from the value '${values.range}')`;
	}

	if (values.range < 10) {
		errors.range = "Skill range must be greater than or equal to 10";
	}

	if (values.range > 100) {
		errors.range = "Skill range must be less than or equal to 100";
	}
	return errors;
};
