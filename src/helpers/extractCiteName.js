export const extractCiteName = (url) => {
	const protocolRemoved = url.replace(/^https?:\/\//, "");
	const wwwRemoved = protocolRemoved.replace(/^www\./, "");
	const domain = wwwRemoved.split("/")[0];
	return domain;
};
