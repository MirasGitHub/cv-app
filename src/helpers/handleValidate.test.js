import { handleValidate } from "./handleValidate";

describe("handleValidate", () => {
	it("should return an empty object for valid input", () => {
		const values = {
			name: "Valid Skill",
			range: "50",
		};

		const errors = handleValidate(values);
		expect(errors).toEqual({});
	});

	it("should return an error for non-numeric range", () => {
		const values = {
			name: "Valid Skill",
			range: "not_a_number",
		};

		const errors = handleValidate(values);
		expect(errors).toEqual({
			range: "Skill range must be a 'number' type",
		});
	});

	it("should return an error for invalid range format", () => {
		const values = {
			name: "Valid Skill",
			range: "20abc",
		};

		const errors = handleValidate(values);
		expect(errors).toEqual({
			range: `Skill range must be a 'number' type but final value was: 'NaN' (cast from the value '20abc')`,
		});
	});

	it("should return an error for range < 10", () => {
		const values = {
			name: "Valid Skill",
			range: "5",
		};

		const errors = handleValidate(values);
		expect(errors).toEqual({
			range: "Skill range must be greater than or equal to 10",
		});
	});

	it("should return an error for range > 100", () => {
		const values = {
			name: "Valid Skill",
			range: "110",
		};

		const errors = handleValidate(values);
		expect(errors).toEqual({
			range: "Skill range must be less than or equal to 100",
		});
	});
});
