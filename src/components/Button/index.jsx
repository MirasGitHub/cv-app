import { Link } from "react-router-dom";
import "./Button.scss";

export const Button = ({ icon, text, path }) => {
	return (
		<div className="button">
			<Link to={path}>
				<button>
					{icon} <p>{text}</p>
				</button>
			</Link>
		</div>
	);
};
