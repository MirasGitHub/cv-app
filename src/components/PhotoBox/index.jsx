import "./PhotoBox.scss";

export const PhotoBox = ({ name, title, description, avatar }) => {
	return (
		<div className="photoBox">
			<div>
				<img src={avatar} alt={`${avatar}'s profile`} />
				<h1>{name}</h1>
			</div>
			{title && <h2>{title}</h2>}
			{description && <p>{description}</p>}
			<br />
		</div>
	);
};
