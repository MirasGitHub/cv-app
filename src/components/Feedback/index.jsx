import { Link } from "react-router-dom";
import "./Feedback.scss";
import profile from "../../assets/images/Ellipse 1.png";
import { Info } from "../Info/index";
import { extractCiteName } from "../../helpers/extractCiteName";

export const Feedback = ({ data }) => (
	<div>
		{data?.map(({ reporter, feedback }) => {
			const citeName = extractCiteName(reporter.citeUrl);
			return (
				<div className="feedback-container" key={reporter.name}>
					<div className="feedback--comment">
						<Info text={feedback} />
					</div>
					<div className="feedback--reporter">
						<img src={profile} alt={reporter.name} />
						<p>
							{reporter.name},
							<Link to={reporter.citeUrl} target="_blank">
								{citeName}
							</Link>
						</p>
					</div>
				</div>
			);
		})}
	</div>
);
