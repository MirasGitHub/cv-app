import "./Panel.scss";
import { Navigation, PhotoBox } from "../index";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import profilePicture from "../../assets/images/miranda.jpeg";

export const Panel = ({ collapsed, onCollapse }) => {
	return (
		<div
			className={`panel ${collapsed ? "collapsed" : ""}`}
			data-testid="panel-container"
		>
			<aside>
				<div className="photo-box-container">
					<PhotoBox name="Miranda Kobalia" avatar={profilePicture} />
				</div>

				<div className="navigation">
					<Navigation />
				</div>
			</aside>
			<button
				onClick={onCollapse}
				className="collapse-btn"
				data-testid="collapse"
			>
				<FontAwesomeIcon icon={faBars} />
			</button>
		</div>
	);
};
