import { render, fireEvent, screen } from "@testing-library/react";
import { Panel } from "./index";
import { Provider } from "react-redux";
import store from "../../app/store";
import { BrowserRouter } from "react-router-dom";

describe("Panel Component", () => {
	it("toggles the 'collapsed' state when 'Panel' button is clicked", () => {
		render(
			<BrowserRouter>
				<Provider store={store}>
					<Panel collapsed={false} onCollapse={() => {}} />
				</Provider>
			</BrowserRouter>
		);
		const panel = screen.getByTestId("panel-container");
		expect(panel).toBeInTheDocument();

		const collapseButton = screen.getByTestId("collapse");
		fireEvent.click(collapseButton);

		expect(panel).toHaveClass("panel");

		fireEvent.click(collapseButton);

		expect(panel).not.toHaveClass("collapsed");
	});
});
