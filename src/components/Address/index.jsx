import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Address.scss";
import { Link } from "react-router-dom";

export const Address = ({ contactInfo }) => {
	return (
		<div className="address-container">
			{contactInfo?.map(({ id, label, url, value, icon }) => (
				<div key={id} className="contact-info">
					<FontAwesomeIcon icon={icon} className="icon" />
					<div>
						<h4>{label}</h4>
						{url ? (
							<Link to={url} target="_blank">
								<p>{value}</p>
							</Link>
						) : (
							<p>{value}</p>
						)}
					</div>
				</div>
			))}
		</div>
	);
};
