import "./Expertise.scss";

export const Expertise = ({ data }) => {
	return (
		<div>
			{data?.map(({ info, date }) => (
				<div className="experience-box" key={info.company}>
					<div className="experience--data">
						<div className="company">
							<h3>{info?.company}</h3>
							<p>{date}</p>
						</div>
						<div className="position">
							<h3>{info?.job}</h3>
							<p>{info.description}</p>
						</div>
					</div>
				</div>
			))}
		</div>
	);
};
