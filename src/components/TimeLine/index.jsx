import "./TimeLine.scss";
import triangle from "../../assets/images/Vector 1.png";
import { useEffect } from "react";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSync } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import {
	fetchEducationsFailure,
	fetchEducationsRequest,
	fetchEducationsSuccess,
} from "../../redux/features/education/educationSlice";

export const TimeLine = () => {
	const dispatch = useDispatch();
	const { data, loading, error } = useSelector((state) => state.educations);

	useEffect(() => {
		const fetchEducations = async () => {
			try {
				dispatch(fetchEducationsRequest());
				const response = await axios.get("/api/educations");
				dispatch(fetchEducationsSuccess(response.data));
			} catch (e) {
				console.error("Error fetching educations:", e);
				dispatch(
					fetchEducationsFailure(
						"Something went wrong; please review your server connection!"
					)
				);
			}
		};

		fetchEducations();
	}, [dispatch]);

	const result = error ? (
		<p className="error-message">{error}</p>
	) : (
		data?.map(({ id, date, title, text }) => (
			<div className="timeline" key={id}>
				<div className="date">
					<h4>{date}</h4>
					<div className="date-decorator"></div>
				</div>
				<div className="timeline--text">
					<div>
						<img src={triangle} alt="triangle" />
						<h3>{title}</h3>
					</div>
					<p>{text}</p>
				</div>
			</div>
		))
	);

	return (
		<div className={loading ? "loading" : "timeline-container"}>
			{loading ? (
				<FontAwesomeIcon className="icon spinner" icon={faSync} />
			) : (
				result
			)}
		</div>
	);
};
