import "./Info.scss";

export const Info = ({ text }) => {
	return (
		<div className="info-container">
			<p>{text}</p>
		</div>
	);
};
