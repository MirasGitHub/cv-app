import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-scroll";
import "./Navigation.scss";
import { Button } from "../Button/index";
import { navigationItems } from "../../data/navigationItems";

export const Navigation = () => {
	return (
		<div className="navigation">
			{navigationItems?.map((item, index) => (
				<Link
					key={index}
					to={item.target}
					spy={true}
					smooth={true}
					offset={-50}
					duration={800}
				>
					<FontAwesomeIcon icon={item.icon} />
					<p>{item.text}</p>
				</Link>
			))}

			<div className="button">
				<Button
					text="Go back"
					icon={<FontAwesomeIcon icon={faChevronLeft} />}
					path="/"
				/>
			</div>
		</div>
	);
};
