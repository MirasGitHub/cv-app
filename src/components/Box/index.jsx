import "./Box.scss";

export const Box = ({ title, content, children }) => {
	return (
		<div className="box">
			<div className="title">
				<h2>{title}</h2>
			</div>
			<div> {content ? <p>{content}</p> : children}</div>  
		</div>
	);
};
