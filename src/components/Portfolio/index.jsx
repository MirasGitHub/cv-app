import { useEffect, useMemo, useState } from "react";
import { portfolioData } from "../../data/index";
import { Link } from "react-router-dom";
import "./Portfolio.scss";
import PortfolioFilter from "./PortfolioFilter";

export const Portfolio = ({ activeTab, setActiveTab }) => {
	const [data, setData] = useState([]);

	useEffect(() => {
		setData(portfolioData);
	}, []);

	const dispayData = useMemo(() => {
		return data.filter(
			(item) => item.type === activeTab || activeTab === "all"
		);
	}, [activeTab, data]);

	return (
		<div>
			<PortfolioFilter activeTab={activeTab} setActiveTab={setActiveTab} />

			<section className="portfolio">
				{dispayData?.map((item) => (
					<div key={item.id}>
						<article className="portfolio-item">
							<div
								className="image"
								style={{
									backgroundImage: `url(${item.image})`,
								}}
							></div>
							<div className="overlay">
								<h1>{item.title}</h1>
								{item.description && <p>{item.description}</p>}
								<Link to={item.link} target="_blank">
									View source
								</Link>
							</div>
						</article>
					</div>
				))}
			</section>
		</div>
	);
};
