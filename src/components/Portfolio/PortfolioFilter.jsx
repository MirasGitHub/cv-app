import "./PortfolioFilter.scss";
import { portfolioFilterData } from "../../data/portfolioFilterData";

const PortfolioFilter = ({ activeTab, setActiveTab }) => {
	return (
		<ul className="tabs">
			{portfolioFilterData?.map((item) => (
				<li
					key={item.id}
					className={`tabs-item ${activeTab === item.type ? "active" : ""}`}
					onClick={() => setActiveTab(item.type)}
				>
					{item.activeName}
				</li>
			))}
		</ul>
	);
};

export default PortfolioFilter;
