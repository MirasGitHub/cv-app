import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchSkills } from "../../../../redux/features/skills/thunks/skillsThunk";
import "./SkillBars.scss";
import ProficiencyScale from "../ProficiencyScale/ProficiencyScale";
import { Circles } from "react-loader-spinner";

const SkillBars = () => {
	const { skillsList, loading } = useSelector((state) => state.skills);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchSkills());
	}, [dispatch]);

	return (
		<div className="skills-list">
			{loading ? (
				<div id="spinner">
					<Circles color="#26c17e" height="50" width="50" />
				</div>
			) : (
				skillsList?.map((skill, index) => (
					<div key={index} className="skill-bar">
						<div
							className="bar"
							style={{
								width: `${skill.skill.range}%`,
							}}
						>
							<p className="skill-name">{skill.skill.name}</p>
						</div>
					</div>
				))
			)}

			<ProficiencyScale />
		</div>
	);
};

export default SkillBars;
