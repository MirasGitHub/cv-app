import { Fragment } from "react";
import "./ProficiencyScale.scss";
import { proficiencyScaleElements } from "../../../../data/proficiencyScaleElements";

const ProficiencyScale = () => {
	return (
		<div className="proficiency-line">
			{proficiencyScaleElements?.map((elem) => (
				<Fragment key={elem.level}>
					<span className="line"></span>
					<div className={`level ${elem.className}`}>{elem.level}</div>
				</Fragment>
			))}
			<span className="line"></span>
		</div>
	);
};

export default ProficiencyScale;
