import { Formik, Field } from "formik";
import "./SkillsForm.scss";
import { handleValidate } from "../../../../helpers/handleValidate";

const SkillsForm = ({ onSubmit }) => {
	const handleSubmit = (values, { resetForm }) => {
		onSubmit(values);
		resetForm();
	};

	return (
		<Formik
			className="formik-container"
			validateOnMount
			initialValues={{ name: "", range: "" }}
			onSubmit={handleSubmit}
			validate={handleValidate}
		>
			{({
				values,
				handleChange,
				handleBlur,
				errors,
				touched,
				handleSubmit,
				resetForm,
			}) => (
				<form
					data-testid="skills-form"
					className="form-container"
					onSubmit={handleSubmit}
				>
					<div>
						<label htmlFor="name">Skill name: </label>
						<Field
							type="text"
							name="name"
							id="name"
							placeholder="Enter skill name"
							className={touched.name && errors.name ? "has-error" : ""}
						/>
					</div>
					{touched.name && errors.name && (
						<div className="error">{errors.name}</div>
					)}
					<div>
						<label htmlFor="range">Skill range: </label>
						<Field
							type="text"
							name="range"
							id="range"
							placeholder="Enter skill range"
							className={touched.range && errors.range ? "has-error" : ""}
						/>
					</div>

					{touched.range && errors.range && (
						<div className="error">{errors.range}</div>
					)}

					<button
						className={`formSubmitBtn ${
							Object.keys(errors).length > 0 ? "disabled" : ""
						}`}
						type="submit"
						disabled={Object.keys(errors).length}
					>
						Add skill
					</button>
				</form>
			)}
		</Formik>
	);
};

export default SkillsForm;
