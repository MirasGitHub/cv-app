import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import SkillsForm from "./SkillsForm";

describe("SkillsForm", () => {
	it("renders the form", () => {
		render(<SkillsForm />);
		const formElement = screen.getByTestId("skills-form");
		expect(formElement).toBeInTheDocument();
	});

	it("disables the submit button when there are errors", async () => {
		render(<SkillsForm />);
		const submitButton = screen.getByText("Add skill");
		const nameInput = screen.getByLabelText("Skill name:");
		const rangeInput = screen.getByLabelText("Skill range:");

		fireEvent.change(nameInput, { target: { value: "New Skill" } });

		fireEvent.change(rangeInput, { target: { value: "invalid" } });

		await waitFor(() => {
			expect(submitButton).toHaveClass("disabled");
		});
	});

	it("handles form submission", async () => {
		const onSubmit = jest.fn();
		render(<SkillsForm onSubmit={onSubmit} />);

		const nameInput = screen.getByLabelText("Skill name:");
		const rangeInput = screen.getByLabelText("Skill range:");
		const submitButton = screen.getByText("Add skill");

		fireEvent.change(nameInput, { target: { value: "New Skill" } });
		fireEvent.change(rangeInput, { target: { value: "75" } });
		fireEvent.click(submitButton);

		await waitFor(() => {
			expect(onSubmit).toHaveBeenCalledTimes(1);
		});
	});
});
