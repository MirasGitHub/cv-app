import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "../Button";
import SkillsForm from "./components/SkillsForm/SkillsForm";
import { faPenToSquare } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { closeForm, openForm } from "../../redux/features/skills/skillsSlice";
import SkillBars from "./components/SkillsBar/SkillBars";
import { addSkill } from "../../redux/features/skills/thunks/skillsThunk";
import "./Skills.scss";

const Skills = () => {
	const isFormOpen = useSelector((state) => state.skills.isOpen);

	const dispatch = useDispatch();

	const toggleForm = () => {
		isFormOpen ? dispatch(closeForm()) : dispatch(openForm());
	};

	const handleFormSubmit = (newSkill) => {
		dispatch(addSkill(newSkill));
	};

	return (
		<div className="skills-container" data-testid="skills-component">
			<div onClick={toggleForm} className="toggle-btn">
				<Button
					text="Open edit"
					icon={<FontAwesomeIcon icon={faPenToSquare} />}
				/>
			</div>
			{isFormOpen && <SkillsForm onSubmit={handleFormSubmit} />}

			<div className="proficiency-levels">
				<SkillBars />
			</div>
		</div>
	);
};

export default Skills;
