import { configureStore } from "@reduxjs/toolkit";
import educationReducer from "../redux/features/education/educationSlice";
import skillsFormReducer from "../redux/features/skills/skillsSlice";

const store = configureStore({
	reducer: {
		educations: educationReducer,
		skills: skillsFormReducer,
	},
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: false,
		}),
});

export default store;
