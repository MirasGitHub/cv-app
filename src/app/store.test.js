import { configureStore } from "@reduxjs/toolkit";
import educationReducer from "../redux/features/education/educationSlice";
import skillsFormReducer from "../redux/features/skills/skillsSlice";

describe("Redux Store Configuration", () => {
	let store;

	beforeEach(() => {
		store = configureStore({
			reducer: {
				educations: educationReducer,
				skillsForm: skillsFormReducer,
			},
		});
	});

	it("should have the educations reducer", () => {
		const state = store.getState();
		expect(state.educations).toBeDefined();
	});

	it("should have the skillsForm reducer", () => {
		const state = store.getState();
		expect(state.skillsForm).toBeDefined();
	});

	it("should have the educations reducer with the correct initial state", () => {
		const state = store.getState().educations;
		expect(state).toEqual({
			data: [],
			loading: false,
			error: null,
		});
	});

	it("should have the skillsForm reducer with the correct initial state", () => {
		const state = store.getState().skillsForm;

		expect(state).toEqual({
			isOpen: false,
			loading: false,
			error: null,
			skillsList: [],
		});
	});
});
