import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import App from "./App";
import { Provider } from "react-redux";
import store from "./app/store";

describe("<App/>", () => {
	test("clicking Home navigates to the Home page", () => {
		render(
			<Provider store={store}>
				<MemoryRouter initialEntries={["/"]}>
					<App />
				</MemoryRouter>
			</Provider>
		);

		fireEvent.click(screen.getByTestId("home-component"));

		expect(window.location.pathname).toBe("/");

		const homeComponent = screen.getByTestId("home-component");
		expect(homeComponent).toBeInTheDocument();
	});

	test("clicking Inner navigates to the Inner page", () => {
		render(
			<Provider store={store}>
				<MemoryRouter initialEntries={["/inner"]}>
					<App />
				</MemoryRouter>
			</Provider>
		);

		fireEvent.click(screen.getByTestId("inner-component"));

		const innerComponent = screen.getByTestId("inner-component");
		expect(innerComponent).toBeInTheDocument();
	});
});
