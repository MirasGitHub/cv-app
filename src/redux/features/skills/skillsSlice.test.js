import skillsFormSlice, { create, openForm, closeForm } from "./skillsSlice";

describe("skillsSlice reducers", () => {
	let state;

	beforeEach(() => {
		state = {
			isOpen: false,
			skillsList: [],
			loading: false,
			error: null,
		};
	});

	it("should handle create", () => {
		const skill = { name: "New Skill", range: 75 };
		const nextState = skillsFormSlice(state, create(skill));
		expect(nextState.skillsList).toContain(skill);
	});

	it("should handle openForm", () => {
		const nextState = skillsFormSlice(state, openForm());
		expect(nextState.isOpen).toBe(true);
	});

	it("should handle closeForm", () => {
		const nextState = skillsFormSlice(state, closeForm());
		expect(nextState.isOpen).toBe(false);
	});
});
