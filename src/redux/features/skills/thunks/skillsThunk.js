import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const fetchSkills = createAsyncThunk(
	"api/skills/fetchSkills",
	async () => {
		try {
			const response = await axios.get("/api/skills");
			return response.data.skills;
		} catch (e) {
			console.error("Error fetching skills: ", e);
		}
	}
);

export const addSkill = createAsyncThunk("skills/addSkill", async (skill) => {
	const response = await axios.post("/api/skills", skill);
	return response.data;
});
