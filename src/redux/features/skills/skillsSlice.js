import { createSlice } from "@reduxjs/toolkit";
import { addSkill, fetchSkills } from "./thunks/skillsThunk";

const initialState = {
	isOpen: false,
	skillsList: [],
	loading: false,
	error: null,
};

const skillsFormSlice = createSlice({
	name: "skills",
	initialState,
	reducers: {
		create: (state, action) => {
			state.skillsList.push(action.payload);
		},
		openForm: (state) => {
			state.isOpen = true;
		},
		closeForm: (state) => {
			state.isOpen = false;
		},
	},
	extraReducers: (builder) => {
		builder
			.addCase(fetchSkills.pending, (state) => {
				state.loading = true;
			})
			.addCase(fetchSkills.fulfilled, (state) => {
				const storedSkills = JSON.parse(localStorage.getItem("skills")) || [];

				state.skillsList = storedSkills;
				state.loading = false;
			})
			.addCase(fetchSkills.rejected, (state, action) => {
				state.loading = false;
				state.error = action.error.message;
			})
			.addCase(addSkill.pending, (state) => {
				state.loading = true;
			})
			.addCase(addSkill.fulfilled, (state, action) => {
				state.skillsList.push(action.payload);

				localStorage.setItem("skills", JSON.stringify(state.skillsList));
				state.loading = false;
			})
			.addCase(addSkill.rejected, (state, action) => {
				state.loading = false;
				state.error = action.error.message;
			});
	},
});

export const { create, openForm, closeForm } = skillsFormSlice.actions;

export default skillsFormSlice.reducer;
