import { createSlice } from "@reduxjs/toolkit";

const initialState = { data: [], loading: false, error: null };

const educationSlice = createSlice({
	name: "educations",
	initialState,
	reducers: {
		fetchEducationsRequest: (state) => {
			state.loading = true;
			state.error = null;
		},
		fetchEducationsSuccess: (state, action) => {
			state.data = action.payload;
			state.loading = false;
			state.error = null;
		},
		fetchEducationsFailure: (state, action) => {
			state.data = [];
			state.loading = false;
			state.error = action.payload;
		},
	},
});

export const {
	fetchEducationsRequest,
	fetchEducationsSuccess,
	fetchEducationsFailure,
} = educationSlice.actions;

export default educationSlice.reducer;
