import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Inner from "./pages/Inner";

function App() {
	return (
		<div className="App" data-testid="app-component">
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/inner" element={<Inner />} />
			</Routes>
		</div>
	);
}

export default App;
