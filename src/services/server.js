import { timeline } from "../data/timeline";
import { Server, Model, Factory, RestSerializer } from "miragejs";

export function makeServer() {
	const server = new Server({
		serializers: {
			application: RestSerializer,
		},

		models: {
			education: Model,
			skill: Model,
		},

		factories: {
			education: Factory.extend({
				date: "Sample Date",
				title: "Sample Title",
				description: "Sample Description",
			}),
			skill: Factory.extend({
				name: "Sample Skill",
				range: 50,
			}),
		},

		routes() {
			this.namespace = "api";

			this.get(
				"/educations",
				() => {
					return timeline;
				},
				{ timing: 3000 }
			);

			this.get(
				"/skills",
				(schema) => {
					const skills = schema.all("skill");
					return skills;
				},
				{ timing: 3000 }
			);

			this.post("/skills", (schema, request) => {
				const newSkill = JSON.parse(request.requestBody);
				const skill = schema.create("skill", newSkill);
				return skill;
			});
		},
	});

	return server;
}
