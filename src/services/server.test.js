import { makeServer } from "./server";
import { timeline } from "../data/timeline";

describe("Mirage Server", () => {
	let server;

	beforeAll(() => {
		server = makeServer();
	});

	afterAll(() => {
		server.shutdown();
	});

	it("should have an education model", () => {
		const educationModel = server.schema.educations;
		expect(educationModel).toBeDefined();
	});

	it("should respond to a GET request to /api/educations with timeline data", async () => {
		const response = await fetch("/api/educations");
		const data = await response.json();
		expect(response.status).toBe(200);
		expect(Array.isArray(data)).toBe(true);
		expect(data).toEqual(timeline);

		expect(data.length).toBe(7);
	});

	it("should respond to a GET request to /api/skills with skill data", async () => {
		const response = await fetch("/api/skills");
		const data = await response.json();
		expect(response.status).toBe(200);
		expect(data.skills).toBeDefined();
	});

	it("should respond to a POST request to /api/skills with a new skill", async () => {
		const newSkill = { name: "New Skill", range: 75 };

		try {
			const response = await fetch("/api/skills", {
				method: "POST",
				body: JSON.stringify(newSkill),
				headers: {
					"Content-Type": "application/json",
				},
			});

			expect(response.status).toBe(201);

			const data = await response.json();

			expect(data.name).toBe(newSkill.name);
			expect(data.range).toBe(newSkill.range);
		} catch (error) {
			console.error("Error in POST request:", error);
		}
	});
});
